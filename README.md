
# Agave JupyterHub and Notebooks #

This is the main repository and project home for efforts aimed at integrating Jupyter notebooks with the Agave platform,
and, in particular, the Agave JupterHub. Integration includes:
  * authorization -- an Agave OAuth based authenticator.
  * notebook spawning -- custom spawner processing.
  * notebook images -- custom images with Agave tools.

See project Jupyter (http://jupyter.org/) and the JupyterHub project (https://github.com/jupyterhub/jupyterhub) for
background.