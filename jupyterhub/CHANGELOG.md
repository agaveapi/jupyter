# Change Log
All notable changes to this project will be documented in this file.

## 1.0.0 - 2017-02-28
### Added
- No change.

### Changed
- (#30) Fix bug in files.getHistory operation where systemId parameter was missing.

### Removed
- No change.


