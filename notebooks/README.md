# Jupyter Notebooks #

This directory contains build files for creating the Jupyter notebook Docker images for each tenant. The base directory
contains common components. The building and tagging of images follows semantic versioning with versions that correspond
to entries in the CHANGELOG. The images belong to the taccsciapps organization on dockerhub, and the image names have the
form `jupyteruser-<tenant>` corresponding to the tenant. For example, `taccsciapps/jupyteruse-ds` is the user notebook
image for the DesignSafe tenant containing tags `1.0.0`, `1.1.0-rc1`, `1.1.0`, etc.

To make changes to a tenant's notebook image, change into the corresponding directory, edit the Dockerfile and/or
other assets, and issue a build. Always build a release candidate first. For example, when working on the `1.1.0`
release of the DesignSafe notebook, issue a build command like this:

    ```shell
    $ docker build -t taccsciapps/jupyteruser-ds:1.1.0rc-1 .
    ```
Next, test your changes locally. Since these notebook images are built to run in JupyterHub, we have to override the
config and command to test locally. A `jupyter-notebook-localconf.py` file should be included in the tenant directory
to facilitate this process. Issue a command like so to run the notebook locally:

    ```shell
    $ run --rm -p 8888:8888 -v $(pwd)/jupyter-notebook-localconf.py:/home/jupyter/.jupyter/jupyter_notebook_config.py taccsciapps/jupyteruser-ds:1.1.0rc-1 start-notebook.sh
    ```

At this point, you should be able to navigate to `http:127.0.0.1:8888` to interact with your notebook.

Once the changes look good, push your rc image to dockerhub:

    ```shell
    $ docker push taccsciapps/jupyteruser-ds:1.1.0rc-1
    ```

You can now use the deployer to deploy the new image to staging.


